(function() {

    var OrdersController = function($scope, $routeParams) {
        var customerId = $routeParams.customerId;
        $scope.orders = null;
    
        function init(){
            //Search the customers for the customerId
            for (var i=0, len=$scope.customers.length; i<len; i++){
                if ($scope.customers[i].id === parseInt(customerId)){
                    $scope.orders = $scope.customers[i].orders;
                    break;
                }
            }
        }
            
        $scope.customers = [{
            id: 1,
            joined: '2000-12-02',
            name: 'John',
            city: 'Chandler',
            orderTotal: 9.9956,
            orders: [{
                id: 1,
                product: 'Shoes',
                total: 9.9956
            }]
        }, {
            id: 2,
            joined: '1965-12-02',
            name: 'Zed',
            city: 'Las Vegas',
            orderTotal: 19.99,
            orders: [{
                id: 2,
                product: 'Baseball',
                total: 9.995
            }, {
                id: 3,
                product: 'Bat',
                total: 9.995
            }]
        }]
    };
    OrdersController.$inject = ['$scope', '$routeParams'];

    angular.module('customersApp')
        .controller('OrdersController', OrdersController);


}());